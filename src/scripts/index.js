import "../styles/index.scss";

const apiKey = "563492ad6f91700001000001844a9fd0c8ea4ce78199d4e9c12df9e9";
const apiURL =
	"https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query=People&per_page=15&page=1";

//AJAX code
$(document).ready(function() {
	$.ajax({
		type: "GET",
		headers: { Authorization: `Bearer ${apiKey}` },
		url: apiURL
	}).done(data => {
		console.log(data);
		renderPhotos(data.photos);
	});
	$("#button").click(function() {
		Search();
	});
});

//render all the photos from the API
function renderPhotos(photos) {
	$.each(photos, (i, photo) => {
		const card = createCard(photo);
		$("#row").append(card);
	});
}

//create card function
function createCard(photo) {
	return `
         <div class="col-4">
           <div class="card h-100" style="18rem;">
             <div class="card-img">
               <img class="card-img-top" src="${photo.src.medium}"/>
               </div>
               <div class="card-body">
               <h5 class="display-10">${photo.photographer}</h5>
              </div>
            </div>
          </div>
             `;
}

function Search() {
	var photoType = $("#search").val();
	console.log(photoType);
	var apiUrl = `https://cors-anywhere.herokuapp.com/https://api.pexels.com/v1/search?query=${photoType}&per_page=15&page=1`;
	$("#row").empty();
	console.log(apiUrl);
	$.ajax({
		type: "GET",
		headers: {
			Authorization: `Bearer ${apiKey}`
		},
		url: apiUrl
	}).done(data => {
		renderPhotos(data.photos);
	});
	return;
}
